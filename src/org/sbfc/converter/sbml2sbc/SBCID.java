/*
 * $Id: XPPID.java 36 2011-05-11 12:21:02Z niko-rodrigue $
 * $URL: https://sbfc.svn.sourceforge.net/svnroot/sbfc/trunk/src/org/sbfc/converter/sbml2xpp/XPPID.java $
 *
 *
 * ==============================================================================
 * Copyright (c) 2010 the copyright is held jointly by the individual
 * authors. See the file AUTHORS for the list of authors
 *
 * This file is part of The System Biology Format Converter (SBFC).
 *
 * SBFC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SBFC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SBFC.  If not, see<http://www.gnu.org/licenses/>.
 * 
 * ==============================================================================
 * 
 */
package org.sbfc.converter.sbml2sbc;

import java.util.HashMap;

/**
 * Checks and Converts the SBML elements id's into compliant SBC id.
 * The same ID pattern is used so it is straightforward. 
 * 
 * @author rodrigue
 * @author Kedar Nath Natarajan
 */
public class SBCID {


	private static HashMap<String, SBCID> SBCIdMap = new HashMap<String, SBCID>();
	private static HashMap<String, SBCID> sbmlIdMap = new HashMap<String, SBCID>();
	
	static {
		sbmlIdMap.put("time", new SBCID("time"));
		sbmlIdMap.put("Time", new SBCID("time"));
		sbmlIdMap.put("floor", new SBCID("floor"));
		//sbmlIdMap.put("default", new SBCID("_default"));
	}

	private String sbmlId;
	private String SBCId;

	public SBCID(String sbmlId) {
		this.sbmlId = sbmlId;
		this.SBCId = sbmlId;
		
	}
	
	public SBCID(String  sbmlId, String SBCId, Boolean diff) {
		this.sbmlId = sbmlId;
		this.SBCId = SBCId;
	}

	public SBCID(String sbmlId, String sbmlReactionId) {
		this.sbmlId = sbmlId + "_" + sbmlReactionId;
		this.SBCId = this.sbmlId; //fixed, AD2014
	}

	/**
	 * Checks that the SBCId comply to the constraints made by SBC.
	 * 
	 * - no checks necessary
	 * 
	 * If necessary, SBCId is updated, and the current SBCID is added to the map
	 * 
	 */
	public void checkSBCId() {

    
    if (sbmlId.equals("default")) this.SBCId = "_default";

		SBCIdMap.put(SBCId, this);
		sbmlIdMap.put(sbmlId, this);
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SBCID) {
			return ((SBCID) obj).getSBCId().equals(SBCId);
		} else if (obj instanceof String) {
			return ((String) obj).equals(SBCId);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return SBCId.hashCode();
	}

	@Override
	public String toString() {
		return SBCId;
	}

	/**
	 * Gets the SBMLid
	 * @return : SBMLid
	 */
	public String getSbmlId() {
		return sbmlId;
	}

	/**
	 * Gets the SBCid
	 * @return : SBCid
	 */
	public String getSBCId() {
		return SBCId;
	}
	


	/**
	 * Gets the SBMLid corresponding to SBCid 
	 * @param SBCId : 
	 * @return : SBMLId
	 */
	public static String getSBMLId(String SBCId) {

		SBCID id = SBCIdMap.get(SBCId);

		if (id == null) {
			return null;
		}

		return id.getSbmlId();
	}

	/**
	 * Gets the SBCid corresponding to SBMLid
	 * @param sbmlId
	 * @return : SBCid 
	 */
	public static String getSBCId(String sbmlId) {

		SBCID id = sbmlIdMap.get(sbmlId);

		if (id == null) {
			return null;
		}

		return id.getSBCId();

	}
	
}
