#!/bin/bash

mkdir -p classes

javac -nowarn -d classes -cp lib/jsbml-1.3.1-with-dependencies.jar src/org/sbfc/converter/*.java src/org/sbfc/converter/exceptions/*.java src/org/sbfc/converter/models/*.java src/org/sbfc/converter/sbml2octave/*.java src/org/sbfc/converter/sbml2sbc/*.java src/org/sbfc/converter/sbml2xpp/*.java

#make the jar

#~ cd classes
#~ jar -xf ../lib/antlr-4.1-complete.jar org
#~ cd ..
#~ jar cfm motifGenerator.jar Manifest.txt -C classes/ .
#~ cd classes
#~ rm -r org
#~ rm -r com
#~ rm -r motifGenerator
#~ rm -r antlr
#~ cd ..
#~ mv motifGenerator.jar build/

#java -cp classes:lib/jsbml-0.8-b2-20110511-with-dependencies.jar org.sbfc.converter.Main

