
## SBML to SBC Converter

    Andrea Degasperi
    Systems Biology Ireland,
    Conway Institute,
    University College Dublin,
    Dublin, Republic of Ireland

This is a fork of the Systems Biology Format Converter (SBFC),
where we add support to convert Systems Biology Markup Language
(SBML) to the Systems Biology Compiler (SBC) input language. 
Given the similarities between XPP input language and SBC input 
language, we begin our work as a fork of this file 

http://sourceforge.net/projects/sbfc/files/converters/sbml2xpp/1.2/sbml2xpp-1.2.zip/download

Their wiki: http://sbfc.sourceforge.net/mediawiki/index.php?title=SBML2XPP

The original source code was stripped down to bare minimal, with
only SBML2XPP and SBML2Octave availabe and SBML2SBC was added.

### Current progress

The converters from SBML to Octave and XPP are left as they were
in the original file. The implementation of the converter from
SBML to SBC is just at the beginning, but we can start by modifying
the SBML2XPP converter as the syntax of SBC has many similarities
with the syntax of XPP.

### Compile

To compile, enter the main project directory and run:

    ./compile.sh

This will generate java bitecode in the directory classes.

### Run

To run the converter, enter the main project directory and run:

    ./run.sh [SBML2Octave|SBML2XPP|SBML2SBC] [filename]

In other words, specify the converter to be used and the name of the
file of the SBML model. For example:

    ./run.sh SBML2SBC mymodel.xml

### Acknowledgements: 

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) under grant
agreement no 613879.

